Some logic circuits that I designed in Digital Systems course in 2017.


• **Asynchronous counter**

Design an asynchronous counter using JK flip-flop (up-counter that count from 0 → 8 then reset to 0)

Answer:
![](demo/tut2_1a.mp4)

• **Synchronous counter**

Design a Mod-8 synchronous counter using JK flip-flop.

Answer:
![](demo/tut2_2a.mp4)

Design a Mod-16 synchronous counter using D flip-flop. The circuit must be count as: 0(start) → 1 → 2 → 3 → 4 → 5 → 6 → 7 → 8 → 9 → 10 → 11 → 0 (return back to start).

Answer:
![](demo/tut2_2b.mp4)

• **8-inputs Multiplexer**

F(A, B, C, D) = SUM(1, 5, 9, 13)

Answer:
![](demo/tut5_4a.mp4)

F(A, B, C, D) = SUM(2, 3, 8, 11)

Answer:
![](demo/tut5_4b.mp4)